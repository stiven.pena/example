package co.com.mainsoft.example.repository;

import co.com.mainsoft.example.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface IProductRepository extends JpaRepository<ProductEntity, Integer> {


    @Query("SELECT product FROM ProductEntity product WHERE product.name= :name AND product.status <> 0")
    Optional<ProductEntity> findByName(String name);

    @Query("SELECT product FROM ProductEntity product WHERE product.productCode= :productCode AND product.status <> 0")
    Optional<ProductEntity> findByProductCode(String productCode);

    @Override
    @Query("select product FROM ProductEntity product WHERE  product.status <> 0")
    List<ProductEntity> findAll();



}
