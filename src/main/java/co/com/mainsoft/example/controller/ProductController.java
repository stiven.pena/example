package co.com.mainsoft.example.controller;


import co.com.mainsoft.example.dto.ProductDto;
import co.com.mainsoft.example.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    IProductService iProductService;
;

    @GetMapping()
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(iProductService.findAll());
    }


    @GetMapping("/ name}")
    public ResponseEntity<?> findByName(@PathVariable String name) {
        return ResponseEntity.ok(iProductService.findByName(name));
    }

    @GetMapping("/product-code/{productCode}")
    public ResponseEntity<?> findByProductCode(@PathVariable String productCode) {
        return ResponseEntity.ok(iProductService.findByProductCode(productCode));
    }


    @PostMapping()
    public ResponseEntity<?> createProduct(@RequestBody ProductDto productDto) {

        try {
            iProductService.createProduct(productDto);
            return new ResponseEntity<>(HttpStatus.CREATED);

        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getCause(), HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updatePRoduct(@PathVariable Integer id, @RequestBody ProductDto productDto) {

        try {
            return new ResponseEntity<>(iProductService.uptadeProduct(id, productDto), HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getCause(), HttpStatus.CONFLICT);

        }
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable Integer id) {

        try {
            iProductService.deleteProduct(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getCause(), HttpStatus.CONFLICT);

        }
    }


}
