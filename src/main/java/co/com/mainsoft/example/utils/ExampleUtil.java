package co.com.mainsoft.example.utils;

import co.com.mainsoft.example.dto.ProductDto;
import co.com.mainsoft.example.entity.ProductEntity;

public class ExampleUtil {


    public static ProductDto productEntityToProductDto(ProductEntity productEntity) {

        return ProductDto.builder()
                .productCode(productEntity.getProductCode())
                .nameProduct(productEntity.getName())
                .id(productEntity.getIdProduct())
                .build();
    }


    public static ProductEntity productDtoToproductEntity(ProductDto productDto) {

        return ProductEntity.builder()
                .productCode(productDto.getProductCode())
                .name(productDto.getNameProduct())
                .status(1)
                .build();
    }


}
