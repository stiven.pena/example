package co.com.mainsoft.example.service;

import co.com.mainsoft.example.dto.ProductDto;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IProductService {


    List<ProductDto> findAll();

    ProductDto findByName(String name);

    ProductDto findByProductCode(String productCode);

    void createProduct(ProductDto productDto) throws Exception;

    ProductDto uptadeProduct(Integer id,ProductDto productDto) throws Exception;

    void deleteProduct(Integer id) throws Exception;
}
