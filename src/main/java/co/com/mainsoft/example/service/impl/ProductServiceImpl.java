package co.com.mainsoft.example.service.impl;


import co.com.mainsoft.example.utils.ExampleUtil;
import co.com.mainsoft.example.dto.ProductDto;
import co.com.mainsoft.example.entity.ProductEntity;
import co.com.mainsoft.example.repository.IProductRepository;
import co.com.mainsoft.example.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements IProductService {


    @Autowired
    IProductRepository iProductRepository;


    @Override
    public List<ProductDto> findAll() {

        List<ProductEntity> productEntityList = iProductRepository.findAll();
        List<ProductDto> productDtoList = new ArrayList<>();

        for (ProductEntity entity : productEntityList) {
            productDtoList.add(ExampleUtil.productEntityToProductDto(entity));
        }
        return productDtoList;
    }


    @Override
    public ProductDto findByName(String name) {

        Optional<ProductEntity> entity = iProductRepository.findByName(name);
        ProductDto productDto = null;

        if (entity.isPresent())
            productDto = ExampleUtil.productEntityToProductDto(entity.get());


        return productDto;
    }


    @Override
    public ProductDto findByProductCode(String productCode) {

        Optional<ProductEntity> entity = iProductRepository.findByProductCode(productCode);
        ProductDto productDto = null;

        if (entity.isPresent())
            productDto = ExampleUtil.productEntityToProductDto(entity.get());

        return productDto;
    }


    @Override
    public void createProduct(ProductDto productDto) throws Exception {

        Optional<ProductEntity> entity = iProductRepository.findByProductCode(productDto.getProductCode());

        if (!entity.isPresent())
            iProductRepository.save(ExampleUtil.productDtoToproductEntity(productDto));
        else
            throw new Exception("El producto ya existe");

    }

    @Override
    public ProductDto uptadeProduct(Integer id, ProductDto productDto) throws Exception {

        Optional<ProductEntity> productEntity = iProductRepository.findById(id);

        if (productEntity.isPresent()) {

            productEntity.get().setName(productDto.getNameProduct());
            productEntity.get().setProductCode(productDto.getProductCode());
            iProductRepository.save(productEntity.get());
            return productDto;

        } else {
            throw new Exception("El producto no existe");
        }


    }

    @Override
    public void deleteProduct(Integer id) throws Exception {

        Optional<ProductEntity> productEntity = iProductRepository.findById(id);

        if (productEntity.isPresent()) {
            productEntity.get().setStatus(0);
            iProductRepository.save(productEntity.get());
        }
        else {
            throw new Exception("El producto no existe");
        }

    }

}
